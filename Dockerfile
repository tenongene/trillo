FROM httpd:2.4
RUN apt-get -y update
WORKDIR /usr/local/apache2/htdocs
COPY . . 
CMD sed -i "s/%%HOSTNAME%%/$HOSTNAME/" index.html && httpd-foreground
